# ---- Base Node ---- #
FROM ubuntu:16.04 AS Base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libevent-dev libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/StealthSend/Stealth.git /opt/stealthcoin && \
    cd /opt/stealthcoin/src && \
    make -f makefile.unix

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8-dev libdb4.8++-dev libssl-dev libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r stealthcoin && useradd -r -m -g stealthcoin stealthcoin
RUN mkdir /data
RUN chown stealthcoin:stealthcoin /data
COPY --from=build /opt/stealthcoin/src/StealthCoind /usr/local/bin/
USER stealthcoin
VOLUME /data
EXPOSE 4437 46502
CMD ["/usr/local/bin/StealthCoind", "-datadir=/data", "-conf=/data/StealthCoin.conf", "-server", "-txindex", "-printtoconsole"]